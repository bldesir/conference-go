from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    url = f"https://api.pexels.com/v1/search"

    response = requests.get(url, params = params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"photo_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"photo_url": None}



def get_weather_data(city, state):
    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geocoding_response = requests.get(geocoding_url)
    geocoding_content = json.loads(geocoding_response.content)

    try:
        lat = geocoding_content[0]["lat"]
        lon = geocoding_content[0]["lon"]
    except (KeyError, IndexError):
        return {"geocoding_url": None}

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_content = json.loads(weather_response.content)
    try:
        return {"weather": weather_content["weather"][0]["description"]}
    except (KeyError, IndexError):
        return {"weather": None}
